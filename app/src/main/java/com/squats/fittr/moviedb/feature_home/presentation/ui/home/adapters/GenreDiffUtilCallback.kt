package com.squats.fittr.moviedb.feature_home.presentation.ui.home.adapters

import androidx.recyclerview.widget.DiffUtil
import com.squats.fittr.moviedb.feature_home.domain.model.Genre

class GenreDiffUtilCallback(
    private val oldList: List<Genre>,
    private val newList: List<Genre>):
    DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].id == newList[newItemPosition].id
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val oldItem = oldList[oldItemPosition]
        val newItem = newList[newItemPosition]
        return when {
            oldItem.name != newItem.name -> {
                false
            }
            oldItem.movies.size != newItem.movies.size -> {
                false
            }
            else -> {
                true
            }
        }

    }
}