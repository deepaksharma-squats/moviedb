package com.squats.fittr.moviedb.feature_home.data.remote.dto

import com.squats.fittr.moviedb.feature_home.data.local.entity.MovieEntity

data class MovieDto(
    val adult: Boolean,
    val backdrop_path: String,
    val genre_ids: List<Int>,
    val id: Int,
    val original_language: String,
    val original_title: String,
    val overview: String,
    val popularity: Double,
    val poster_path: String,
    val release_date: String,
    val title: String,
    val video: Boolean,
    val vote_average: Double,
    val vote_count: Int
) {
    fun toMovieEntity(): MovieEntity {
        return MovieEntity(
            movieId = id,
            title = title,
            date = release_date,
            posterPath = poster_path
        )
    }
}