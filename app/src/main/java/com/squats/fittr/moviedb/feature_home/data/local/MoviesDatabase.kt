package com.squats.fittr.moviedb.feature_home.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.squats.fittr.moviedb.feature_home.data.local.entity.GenreEntity
import com.squats.fittr.moviedb.feature_home.data.local.entity.GenreMovieCrossRef
import com.squats.fittr.moviedb.feature_home.data.local.entity.MovieEntity
import com.squats.fittr.moviedb.feature_home.data.local.entity.MoviePagingEntity

@Database(
    entities = [
        GenreEntity::class,
        MovieEntity::class,
        GenreMovieCrossRef::class,
        MoviePagingEntity::class
    ],
    version = 1
)
abstract class MoviesDatabase : RoomDatabase() {
    abstract val moviesDao: MoviesDao
}