package com.squats.fittr.moviedb.feature_home.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squats.fittr.moviedb.feature_home.domain.model.Movie

@Entity
data class MovieEntity(
    @PrimaryKey(autoGenerate = false) val movieId: Int,
    val title: String,
    val date: String,
    val posterPath: String
) {
    fun toMovie(): Movie {
        return Movie(
            id = movieId,
            posterPath = posterPath,
            title = title
        )
    }
}