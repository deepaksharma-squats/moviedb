package com.squats.fittr.moviedb.feature_home.data.local

import androidx.room.*
import com.squats.fittr.moviedb.feature_home.data.local.entity.*

@Dao
interface MoviesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGenres(genres: List<GenreEntity>)

    @Query("DELETE FROM genreentity")
    suspend fun deleteAllGenres()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMovies(movies: List<MovieEntity>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMoviePaging(pagingEntity: MoviePagingEntity)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertGenreMovieCrossRef(crossRef: GenreMovieCrossRef)

    @Transaction
    @Query("SELECT * FROM movieentity WHERE movieId = :movieId ")
    suspend fun getMovie(movieId: Int): List<MovieEntity>

    @Transaction
    @Query("SELECT * FROM genreentity WHERE genreId = :genreId ")
    suspend fun getGenreAndMoviesWithGenreId(genreId: Int): List<GenreWithMoviesEntity>

    @Transaction
    @Query("SELECT * FROM genreentity")
    suspend fun getAllGenresWithMoviesList(): List<GenreWithMoviesEntity>

    @Transaction
    @Query("SELECT * FROM movieentity WHERE movieId = :movieId ")
    suspend fun getGenreAndMoviesWithMovieId(movieId: Int): List<MoviesWithGenresEntity>
}