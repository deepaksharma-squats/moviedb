package com.squats.fittr.moviedb.feature_home.data.repository

import com.squats.fittr.moviedb.core.util.Resource
import com.squats.fittr.moviedb.feature_home.data.local.MoviesDao
import com.squats.fittr.moviedb.feature_home.domain.model.Movie
import com.squats.fittr.moviedb.feature_home.domain.repository.MovieDetailRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class MovieDetailRepositoryImpl(
    private val dao: MoviesDao
): MovieDetailRepository {
    override fun getMovie(movieId: Int): Flow<Resource<Movie>> = flow {
        emit(Resource.Loading())

        val movie = dao.getMovie(movieId).firstOrNull()
        if (movie != null) {
            emit(Resource.Loading(data = movie.toMovie()))
        } else {
            emit(Resource.Error(
                message = "Oops, something went wrong!"
            ))
        }
    }



}