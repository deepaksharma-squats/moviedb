package com.squats.fittr.moviedb.feature_home.domain.use_case

import com.squats.fittr.moviedb.core.util.Resource
import com.squats.fittr.moviedb.feature_home.domain.model.Genre
import com.squats.fittr.moviedb.feature_home.domain.repository.GenreRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetGenres @Inject constructor(
    private val repository: GenreRepository
) {
    operator fun invoke(): Flow<Resource<List<Genre>>> {
        return repository.getGenreList()
    }
}