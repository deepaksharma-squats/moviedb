package com.squats.fittr.moviedb.feature_home.domain.repository

import com.squats.fittr.moviedb.core.util.Resource
import com.squats.fittr.moviedb.feature_home.domain.model.Genre
import kotlinx.coroutines.flow.Flow

interface GenreRepository {
    fun getGenreList(): Flow<Resource<List<Genre>>>
}
