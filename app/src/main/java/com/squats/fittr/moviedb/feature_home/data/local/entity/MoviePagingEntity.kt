package com.squats.fittr.moviedb.feature_home.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MoviePagingEntity(
    @PrimaryKey(autoGenerate = false) val genreId: Int,
    val page: Int,
    val totalPages: Int,
    val totalResults: Int
)
