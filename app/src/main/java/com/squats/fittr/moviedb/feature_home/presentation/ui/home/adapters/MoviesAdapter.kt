package com.squats.fittr.moviedb.feature_home.presentation.ui.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squats.fittr.moviedb.databinding.MovieViewBinding
import com.squats.fittr.moviedb.feature_home.domain.model.Movie

class MoviesAdapter(private var items: List<Movie>, private val handler: MovieItemClickHandler): RecyclerView.Adapter<MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        return MovieViewHolder(MovieViewBinding.inflate(LayoutInflater.from(parent.context), parent, false), handler)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.updateValuesInUI(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(items: List<Movie>) {
        this.items = items
        notifyItemRangeChanged(0, items.size)
    }
}