package com.squats.fittr.moviedb.feature_home.presentation.ui.home.adapters

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.squats.fittr.moviedb.databinding.MovieViewBinding
import com.squats.fittr.moviedb.feature_home.domain.model.Movie

class MovieViewHolder(private val binding: MovieViewBinding, private val handler: MovieItemClickHandler): RecyclerView.ViewHolder(binding.root),
    View.OnClickListener {
    init {
        binding.root.setOnClickListener(this)
    }
    fun updateValuesInUI(movie: Movie) {
        binding.movie = movie
    }

    override fun onClick(p0: View?) {
        binding.movie?.id?.let {
            handler.onItemClicked(it)
        }
    }
}