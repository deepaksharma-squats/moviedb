package com.squats.fittr.moviedb.feature_home.domain.use_case

import com.squats.fittr.moviedb.core.util.Resource
import com.squats.fittr.moviedb.feature_home.domain.model.Genre
import com.squats.fittr.moviedb.feature_home.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMovies(
    private val repository: MovieRepository
) {

    operator fun invoke(genreId: Int): Flow<Resource<List<Genre>>> {
        return repository.getMovieList(genreId)
    }
}