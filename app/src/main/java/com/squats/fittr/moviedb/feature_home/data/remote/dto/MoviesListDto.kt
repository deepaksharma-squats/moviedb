package com.squats.fittr.moviedb.feature_home.data.remote.dto

data class MoviesListDto(
    val id: Int,
    val page: Int,
    val results: List<MovieDto>,
    val total_pages: Int,
    val total_results: Int
)