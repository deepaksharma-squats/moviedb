package com.squats.fittr.moviedb.feature_home.data.remote

import com.squats.fittr.moviedb.feature_home.data.remote.dto.GenresListDto
import com.squats.fittr.moviedb.feature_home.data.remote.dto.MoviesListDto
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface HomeApi {
    @GET("genre/movie/list")
    suspend fun getGenres(
        @Query("api_key") api_key: String = API_KEY,
    ): GenresListDto

    @GET("genre/{genre_id}/movies")
    suspend fun getMovies(
        @Path("genre_id") genreId: Int,
        @Query("api_key") word: String = API_KEY
    ): MoviesListDto

    companion object {
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val API_KEY = "f17e9c5e6c34ad9dc2bf6aab852c0cc7"
        const val IMAGE_BASE = "https://image.tmdb.org/t/p/w500"
    }
}