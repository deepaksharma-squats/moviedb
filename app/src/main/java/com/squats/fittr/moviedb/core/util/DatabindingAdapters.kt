package com.squats.fittr.moviedb.core.util

import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.signature.ObjectKey

@BindingAdapter(value = ["android:src", "placeholderImage", "errorImage", "usescale", "signature"], requireAll = false)
fun loadImageWithGlide(imageView: ImageView, obj: Any?, placeholder: Any?, errorImage: Any?, useScale: Boolean = false, signature: String?) {

    if (obj is Int) {
        imageView.setImageResource(obj)
        return
    }

    val options = if (signature == null) RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL)
    else RequestOptions().diskCacheStrategy(DiskCacheStrategy.ALL).signature(ObjectKey(signature))

    if (placeholder is Drawable) options.placeholder(placeholder)
    else if (placeholder is Int) options.placeholder(placeholder)
    if (errorImage is Drawable) options.error(errorImage)
    else if (errorImage is Int) options.error(errorImage)

    if (useScale) {
        when (imageView.scaleType) {
            ImageView.ScaleType.FIT_CENTER -> options.fitCenter()
            ImageView.ScaleType.CENTER_CROP -> options.centerCrop()
            ImageView.ScaleType.CENTER_INSIDE ->  options.centerInside()
        }
    }

    Glide.with(imageView.context)
        .load(obj)
        .thumbnail(0.25f)
        .apply(options)
        .into(imageView)
}