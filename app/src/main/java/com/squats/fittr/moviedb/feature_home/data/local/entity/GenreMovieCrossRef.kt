package com.squats.fittr.moviedb.feature_home.data.local.entity

import androidx.room.Entity

@Entity(primaryKeys = ["genreId", "movieId"])
data class GenreMovieCrossRef(
    val genreId: Int,
    val movieId: Int
)
