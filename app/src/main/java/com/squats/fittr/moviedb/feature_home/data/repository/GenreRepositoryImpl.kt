package com.squats.fittr.moviedb.feature_home.data.repository

import com.squats.fittr.moviedb.core.util.Resource
import com.squats.fittr.moviedb.feature_home.data.local.MoviesDao
import com.squats.fittr.moviedb.feature_home.data.remote.HomeApi
import com.squats.fittr.moviedb.feature_home.domain.model.Genre
import com.squats.fittr.moviedb.feature_home.domain.repository.GenreRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class GenreRepositoryImpl(
    private val api: HomeApi,
    private val dao: MoviesDao
): GenreRepository {

    override fun getGenreList(): Flow<Resource<List<Genre>>> = flow {
        emit(Resource.Loading())

        val genres = dao.getAllGenresWithMoviesList().map { it.toGenreWithMovie() }
        emit(Resource.Loading(data = genres))

        try {
            val remoteGenreList = api.getGenres()
            dao.deleteAllGenres()
            dao.insertGenres(genres = remoteGenreList.genres.map { it.toGenreEntity() })
        } catch(e: HttpException) {
            e.printStackTrace()
            emit(Resource.Error(
                message = "Oops, something went wrong!",
                data = genres
            ))
        } catch(e: IOException) {
            e.printStackTrace()
            emit(Resource.Error(
                message = "Couldn't reach server, check your internet connection.",
                data = genres
            ))
        }

        val newGenres = dao.getAllGenresWithMoviesList().map { it.toGenreWithMovie() }
        emit(Resource.Success(newGenres))
    }
}