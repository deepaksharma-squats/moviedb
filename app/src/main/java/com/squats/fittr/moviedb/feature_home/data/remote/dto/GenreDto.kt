package com.squats.fittr.moviedb.feature_home.data.remote.dto

import com.squats.fittr.moviedb.feature_home.data.local.entity.GenreEntity
import com.squats.fittr.moviedb.feature_home.domain.model.Genre

data class GenreDto(
    val id: Int,
    val name: String
) {
    fun toGenreEntity() : GenreEntity {
        return GenreEntity(
            genreId = id,
            name = name
        )
    }
}