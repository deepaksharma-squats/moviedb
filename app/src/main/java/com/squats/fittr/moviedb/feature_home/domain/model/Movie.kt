package com.squats.fittr.moviedb.feature_home.domain.model

import com.squats.fittr.moviedb.feature_home.data.remote.HomeApi

data class Movie(
    val id: Int,
    val posterPath: String,
    val title: String
) {
    val fullPosterPath: String = "${HomeApi.IMAGE_BASE}$posterPath"
}