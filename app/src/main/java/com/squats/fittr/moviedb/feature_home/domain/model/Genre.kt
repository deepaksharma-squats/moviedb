package com.squats.fittr.moviedb.feature_home.domain.model

data class Genre(
    val id: Int,
    val name: String,
    val movies: List<Movie> = emptyList()
)