package com.squats.fittr.moviedb.feature_home.presentation.ui.home

import com.squats.fittr.moviedb.feature_home.domain.model.Genre
import com.squats.fittr.moviedb.feature_home.domain.model.Movie

data class GenreState(
        val items: List<Movie> = emptyList(),
        val isLoading: Boolean = false
 )
