package com.squats.fittr.moviedb.feature_home.domain.use_case

import com.squats.fittr.moviedb.core.util.Resource
import com.squats.fittr.moviedb.feature_home.domain.model.Movie
import com.squats.fittr.moviedb.feature_home.domain.repository.MovieDetailRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMovieDetail @Inject constructor(
    private val repository: MovieDetailRepository
) {

    operator fun invoke(movieId: Int): Flow<Resource<Movie>> {
        return repository.getMovie(movieId)
    }
}