package com.squats.fittr.moviedb.feature_home.data.repository

import com.squats.fittr.moviedb.core.util.Resource
import com.squats.fittr.moviedb.feature_home.data.local.MoviesDao
import com.squats.fittr.moviedb.feature_home.data.local.entity.GenreMovieCrossRef
import com.squats.fittr.moviedb.feature_home.data.local.entity.MoviePagingEntity
import com.squats.fittr.moviedb.feature_home.data.remote.HomeApi
import com.squats.fittr.moviedb.feature_home.domain.model.Genre
import com.squats.fittr.moviedb.feature_home.domain.repository.MovieRepository
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException

class MovieRepositoryImpl(
    private val api: HomeApi,
    private val dao: MoviesDao
): MovieRepository {


    override fun getMovieList(genreId: Int): Flow<Resource<List<Genre>>> = flow {
        emit(Resource.Loading())

        val genres = dao.getGenreAndMoviesWithGenreId(genreId).map { it.toGenreWithMovie() }
        emit(Resource.Loading(data = genres))

        try {
            val remoteGenreList = api.getMovies(genreId)
            dao.insertMovies(remoteGenreList.results.map { it.toMovieEntity() })
            remoteGenreList.results.forEach { movieDto ->
                movieDto.genre_ids.forEach { genreId ->
                    dao.insertGenreMovieCrossRef(GenreMovieCrossRef(
                        genreId = genreId,
                        movieId = movieDto.id
                    ))
                }
            }
            val pagingEntity = MoviePagingEntity(
                genreId = remoteGenreList.id,
                page = remoteGenreList.page,
                totalPages = remoteGenreList.total_pages,
                totalResults = remoteGenreList.total_results
            )
            dao.insertMoviePaging(pagingEntity)
        } catch(e: HttpException) {
            emit(
                Resource.Error(
                    message = "Oops, something went wrong!",
                    data = genres
                ))
        } catch(e: IOException) {
            emit(
                Resource.Error(
                    message = "Couldn't reach server, check your internet connection.",
                    data = genres
                ))
        }

        val newGenres = dao.getGenreAndMoviesWithGenreId(genreId).map { it.toGenreWithMovie() }
        emit(Resource.Success(newGenres))
    }
}