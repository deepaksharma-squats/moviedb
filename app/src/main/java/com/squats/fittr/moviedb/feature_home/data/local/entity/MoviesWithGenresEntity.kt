package com.squats.fittr.moviedb.feature_home.data.local.entity

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation

data class MoviesWithGenresEntity(
    @Embedded val movie: MovieEntity,
    @Relation(
        parentColumn = "movieId",
        entityColumn = "genreId",
        associateBy = Junction(GenreMovieCrossRef::class)
    ) val genres: List<GenreEntity>
)