package com.squats.fittr.moviedb.feature_home.presentation.ui.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.snackbar.Snackbar
import com.squats.fittr.moviedb.databinding.FragmentDetailBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class DetailFragment: Fragment() {
    lateinit var binding: FragmentDetailBinding
    private var viewModel = viewModels<MovieDetailViewModel>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        val movieId = DetailFragmentArgs.fromBundle(requireArguments()).movieId
        viewModel.value.getMovieDetail(movieId)
        observeValues()
    }

    private fun observeValues() {
        lifecycleScope.launchWhenStarted {
            viewModel.value.stateFlow.collectLatest {
//                binding.isLoading = it.isLoading
                binding.movie = it.item
            }

            viewModel.value.eventFlow.collectLatest { event ->
                when(event) {
                    is MovieDetailViewModel.UIEvent.ShowSnackBar -> {
                        Snackbar.make(binding.root, event.message, Snackbar.LENGTH_LONG).show()
                    }
                }
            }
        }
    }
}