package com.squats.fittr.moviedb.feature_home.presentation.ui.home.adapters

import androidx.recyclerview.widget.RecyclerView
import com.squats.fittr.moviedb.databinding.GenreViewBinding
import com.squats.fittr.moviedb.feature_home.domain.model.Genre

class GenreViewHolder(private val binding: GenreViewBinding, private val handler: MovieItemClickHandler): RecyclerView.ViewHolder(binding.root) {
    private val moviesAdapter = MoviesAdapter(emptyList(), handler)
    init {
        binding.adapter = moviesAdapter
    }
    fun updateValuesInUI(genre: Genre) {
        binding.name = genre.name
        moviesAdapter.setItems(genre.movies)
    }
}