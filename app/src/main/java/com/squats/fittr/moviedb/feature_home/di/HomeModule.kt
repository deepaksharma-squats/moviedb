package com.squats.fittr.moviedb.feature_home.di

import android.app.Application
import androidx.room.Room
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.google.gson.Gson
import com.squats.fittr.moviedb.feature_home.data.local.MoviesDatabase
import com.squats.fittr.moviedb.feature_home.data.remote.HomeApi
import com.squats.fittr.moviedb.feature_home.data.repository.GenreRepositoryImpl
import com.squats.fittr.moviedb.feature_home.data.repository.MovieDetailRepositoryImpl
import com.squats.fittr.moviedb.feature_home.data.repository.MovieRepositoryImpl
import com.squats.fittr.moviedb.feature_home.domain.repository.GenreRepository
import com.squats.fittr.moviedb.feature_home.domain.repository.MovieDetailRepository
import com.squats.fittr.moviedb.feature_home.domain.repository.MovieRepository
import com.squats.fittr.moviedb.feature_home.domain.use_case.GetGenres
import com.squats.fittr.moviedb.feature_home.domain.use_case.GetMovieDetail
import com.squats.fittr.moviedb.feature_home.domain.use_case.GetMovies
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object HomeModule {

    @Provides
    @Singleton
    fun provideGetGenresUseCase(repository: GenreRepository): GetGenres {
        return GetGenres(repository)
    }

    @Provides
    @Singleton
    fun provideGetMoviesUseCase(repository: MovieRepository): GetMovies {
        return GetMovies(repository)
    }

    @Provides
    @Singleton
    fun provideGetMovieDetailUseCase(repository: MovieDetailRepository): GetMovieDetail {
        return GetMovieDetail(repository)
    }

    @Provides
    @Singleton
    fun provideGenreRepository(homeApi: HomeApi, database: MoviesDatabase): GenreRepository {
        return GenreRepositoryImpl(homeApi, database.moviesDao)
    }

    @Provides
    @Singleton
    fun provideMoviesRepository(homeApi: HomeApi, database: MoviesDatabase): MovieRepository {
        return MovieRepositoryImpl(homeApi, database.moviesDao)
    }

    @Provides
    @Singleton
    fun provideMoviesDetailRepository(homeApi: HomeApi, database: MoviesDatabase): MovieDetailRepository {
        return MovieDetailRepositoryImpl(database.moviesDao)
    }

    @Provides
    @Singleton
    fun provideMoviesDatabase(app: Application): MoviesDatabase {
        return Room.databaseBuilder(
            app, MoviesDatabase::class.java, "movies_db"
        ).build()
    }

    @Provides
    @Singleton
    fun provideHomeApi(): HomeApi {
        val client = OkHttpClient.Builder()
            .addNetworkInterceptor(StethoInterceptor())
            .build()

        return Retrofit.Builder()
            .baseUrl(HomeApi.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(HomeApi::class.java)
    }
}