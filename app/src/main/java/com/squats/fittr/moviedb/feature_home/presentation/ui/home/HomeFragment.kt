package com.squats.fittr.moviedb.feature_home.presentation.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.squats.fittr.moviedb.databinding.FragmentHomeBinding
import com.squats.fittr.moviedb.feature_home.domain.model.Genre
import com.squats.fittr.moviedb.feature_home.presentation.ui.home.adapters.GenreAdapter
import com.squats.fittr.moviedb.feature_home.presentation.ui.home.adapters.MovieItemClickHandler
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest

@AndroidEntryPoint
class HomeFragment : Fragment(), MovieItemClickHandler {
    lateinit var binding: FragmentHomeBinding
    private var viewModel = viewModels<HomeViewModel>()
    private var adapter = GenreAdapter(ArrayList() ,this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.adapter = adapter
        observeValues()
    }

    private fun observeValues() {
        lifecycleScope.launchWhenStarted {
            viewModel.value.stateFlow.collectLatest {
                binding.isLoading = it.isLoading
                adapter.setItems(ArrayList(it.items))
            }

            viewModel.value.eventFlow.collectLatest { event ->
                when(event) {
                    is HomeViewModel.UIEvent.ShowSnackBar -> {
                        Snackbar.make(binding.root, event.message, Snackbar.LENGTH_LONG).show()
                    }
                    is HomeViewModel.UIEvent.MovieListUpdated -> {
                        updateItemsForGenre(event.genre)
                    }
                }
            }
        }
    }

    private fun updateItemsForGenre(genre: Genre?) {
        genre?.let { adapter.replaceGenre(it) }
    }

    private fun navigateToDetail(movieId: Int) {
        findNavController().navigate(
            HomeFragmentDirections.actionHomeFragmentToDetailFragment(movieId)
        )
    }

    override fun onItemClicked(id: Int) {
       navigateToDetail(id)
    }
}