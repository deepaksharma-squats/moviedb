package com.squats.fittr.moviedb.feature_home.presentation.ui.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.squats.fittr.moviedb.databinding.GenreViewBinding
import com.squats.fittr.moviedb.feature_home.domain.model.Genre

class GenreAdapter(private var items: ArrayList<Genre>, private val handler: MovieItemClickHandler): RecyclerView.Adapter<GenreViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenreViewHolder {
        return GenreViewHolder(GenreViewBinding.inflate(LayoutInflater.from(parent.context), parent, false), handler)
    }

    override fun onBindViewHolder(holder: GenreViewHolder, position: Int) {
        holder.updateValuesInUI(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(newItems: ArrayList<Genre>) {
        val callback = GenreDiffUtilCallback(this.items, newItems)
        val diffResult = DiffUtil.calculateDiff(callback)
        this.items = newItems
        diffResult.dispatchUpdatesTo(this)
    }

    fun replaceGenre(genre: Genre) {
        val index = items.indexOfFirst { it.id == genre.id }
        if (index != -1) {
            items[index] = genre
            notifyItemChanged(index)
        }
    }
}