package com.squats.fittr.moviedb.feature_home.presentation.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.squats.fittr.moviedb.core.util.Resource
import com.squats.fittr.moviedb.feature_home.domain.model.Genre
import com.squats.fittr.moviedb.feature_home.domain.use_case.GetGenres
import com.squats.fittr.moviedb.feature_home.domain.use_case.GetMovies
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getGenres: GetGenres,
    private val getMovies: GetMovies
) : ViewModel() {

    private val _state: MutableStateFlow<HomeState> = MutableStateFlow(HomeState())
    val stateFlow = _state.asStateFlow()

    private val _eventFlow = MutableSharedFlow<UIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    init {
        getGenresList()
    }

    private fun getGenresList() {
        viewModelScope.launch {
            getGenres()
                .onEach { result ->
                    when (result) {
                        is Resource.Success -> {
                            _state.value = stateFlow.value.copy(
                                items = result.data ?: emptyList(),
                                isLoading = false
                            )
                            result.data?.forEach {  genre ->
                                getMovieList(genre.id)
                            }
                        }
                        is Resource.Error -> {
                            _state.value = stateFlow.value.copy(
                                items = result.data ?: emptyList(),
                                isLoading = false
                            )
                            _eventFlow.emit(
                                UIEvent.ShowSnackBar(
                                    result.message ?: "Unknown error"
                                )
                            )
                        }
                        is Resource.Loading -> {
                            _state.value = stateFlow.value.copy(
                                items = result.data ?: emptyList(),
                                isLoading = true
                            )
                        }
                    }
                }.launchIn(this)
        }
    }

    private fun getMovieList(genreId: Int) {
        viewModelScope.launch {
            getMovies(genreId)
                .onEach { result ->
                    when (result) {
                        is Resource.Success -> {
                            _eventFlow.emit(
                                UIEvent.MovieListUpdated(
                                    genre = (result.data ?: emptyList()).firstOrNull()
                                )
                            )
                        }
                    }
                }.launchIn(this)
        }
    }

    sealed class UIEvent {
        data class ShowSnackBar(val message: String) : UIEvent()
        data class MovieListUpdated(val genre: Genre?) : UIEvent()
    }
}