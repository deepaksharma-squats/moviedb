package com.squats.fittr.moviedb.feature_home.presentation.ui.detail;

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.squats.fittr.moviedb.core.util.Resource
import com.squats.fittr.moviedb.feature_home.domain.use_case.GetMovieDetail
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val useCase: GetMovieDetail
) : ViewModel() {

    private val _state: MutableStateFlow<DetailState> = MutableStateFlow(DetailState())
    val stateFlow = _state.asStateFlow()

    private val _eventFlow = MutableSharedFlow<UIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    fun getMovieDetail(movieId: Int) {
        viewModelScope.launch {
            useCase(movieId).onEach { result ->
                    when (result) {
                        is Resource.Success -> {
                            _state.value = stateFlow.value.copy(
                                item = result.data,
                                isLoading = false
                            )
                        }
                        is Resource.Error -> {
                            _state.value = stateFlow.value.copy(
                                item = result.data,
                                isLoading = false
                            )
                            _eventFlow.emit(
                                UIEvent.ShowSnackBar(
                                    result.message ?: "Unknown error"
                                )
                            )
                        }
                        is Resource.Loading -> {
                            _state.value = stateFlow.value.copy(
                                item = result.data,
                                isLoading = true
                            )
                        }
                    }
                }.launchIn(this)
        }
    }

    sealed class UIEvent {
        data class ShowSnackBar(val message: String) : UIEvent()
    }
}