package com.squats.fittr.moviedb.feature_home.data.remote.dto

data class GenresListDto(
    val genres: List<GenreDto>
)