package com.squats.fittr.moviedb.feature_home.domain.repository

import com.squats.fittr.moviedb.core.util.Resource
import com.squats.fittr.moviedb.feature_home.domain.model.Genre
import com.squats.fittr.moviedb.feature_home.domain.model.Movie
import kotlinx.coroutines.flow.Flow

interface MovieRepository {
    fun getMovieList(genreId: Int): Flow<Resource<List<Genre>>>
}
