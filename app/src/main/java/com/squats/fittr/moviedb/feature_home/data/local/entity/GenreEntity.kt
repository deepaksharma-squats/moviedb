package com.squats.fittr.moviedb.feature_home.data.local.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squats.fittr.moviedb.feature_home.domain.model.Genre

@Entity
data class GenreEntity(
    @PrimaryKey(autoGenerate = false) val genreId: Int,
    val name: String,
) {
    fun toGenre() : Genre {
        return Genre(
            id = genreId,
            name = name
        )
    }
}