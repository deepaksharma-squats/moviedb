package com.squats.fittr.moviedb.feature_home.presentation.ui.detail

import com.squats.fittr.moviedb.feature_home.domain.model.Movie

data class DetailState(
    val item: Movie? = null,
    val isLoading: Boolean = false
)

