package com.squats.fittr.moviedb.feature_home.presentation.ui.home

import com.squats.fittr.moviedb.feature_home.domain.model.Genre

data class HomeState(
    val items: List<Genre> = emptyList(),
    val isLoading: Boolean = false
 )
