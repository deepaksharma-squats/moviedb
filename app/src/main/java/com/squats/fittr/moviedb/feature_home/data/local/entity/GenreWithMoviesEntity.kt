package com.squats.fittr.moviedb.feature_home.data.local.entity

import androidx.room.Embedded
import androidx.room.Junction
import androidx.room.Relation
import com.squats.fittr.moviedb.feature_home.domain.model.Genre

data class GenreWithMoviesEntity(
    @Embedded val genre: GenreEntity,
    @Relation(
        parentColumn = "genreId",
        entityColumn = "movieId",
        associateBy = Junction(GenreMovieCrossRef::class)
    )
    val movies: List<MovieEntity>
) {
    fun toGenreWithMovie(): Genre {
        return Genre(
            id = genre.genreId,
            name = genre.name,
            movies = movies.map { it.toMovie() }
        )
    }
}
