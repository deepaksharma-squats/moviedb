package com.squats.fittr.moviedb.feature_home.presentation.ui.home.adapters

interface MovieItemClickHandler {
    fun onItemClicked(id: Int)
}